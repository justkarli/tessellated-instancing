# Tessllated - Instancing scene
This project should be a simple scene demonstrating an AMD bug. 
The scene consist of a triangle defined in the vertex shader, that is tessellated and positioned via instancing. 

![alt working example](img/working_example.png)

As soon as I define layout qualifiers for all shader signatures, the triangles are not visible in the scene for AMD cards but it is working on nvidia & intel gpu's. 

The only thing I recognized when debugging the scene with renderdoc was that it seems that some variables of the shader signature are on the same registry as other variables, although they have different type?

![alt tcs renderdoc](img/tcs_signatures.png)
