#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <sstream>

void framebufferResizeCallback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);
void glfwKeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods);
void glewErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const GLvoid *userParam);
void glfwErrorCallback(int error, const char* description);

bool wireFrameEnabled = false;

static const char * vs_source[] =
{
	"#version 460 core                                                  \n"
	"                                                                   \n"
	"in mat4 modelMatrix;							\n"
	"																	\n"
	"out gl_PerVertex													\n"
	"{																	\n"
		"vec4 gl_Position;												\n"
		"float gl_PointSize;											\n"
		"float gl_ClipDistance[];										\n"
	"};																	\n"
	"																	\n"
	"struct vertexOut													\n"
	"{																	\n"
	"	mat4 mv_matrix;													\n"
	"};																	\n"
	"																	\n"
	"layout(location = 0) out mat4 mv_matrix;							\n"
	"                                                                   \n"
	"void main(void)                                                    \n"
	"{                                                                  \n"
	"    const vec4 vertices[] = vec4[](vec4( 0.25, -0.25, 0.5, 1.0),   \n"
	"                                   vec4(-0.25, -0.25, 0.5, 1.0),   \n"
	"                                   vec4( 0.25,  0.25, 0.5, 1.0));  \n"
	"                                                                   \n"
	"   gl_Position = vertices[gl_VertexID];                            \n"
	"	mv_matrix = modelMatrix;									\n"
	"}                                                                  \n"
};

static const char * tcs_source[] =
{
	"#version 460 core                                                                 \n"
	"                                                                                  \n"
	"layout (vertices = 3) out;                                                        \n"
	"                                                                                  \n"
	"                                                                                  \n"
	"struct vertex																	   \n"
	"{																				   \n"
	"	mat4 mv_matrix;																   \n"
	"};																				   \n"
	"																				   \n"
	"in gl_PerVertex																   \n"
	"{																				   \n"
		"vec4 gl_Position;															   \n"
		"float gl_PointSize;														   \n"
		"float gl_ClipDistance[];													   \n"
	"} gl_in[];																		   \n"
	"																				   \n"
	"layout(location = 0) in mat4 mv_matrix[];										   \n"
	"																				   \n"
	"out gl_PerVertex																   \n"
	"{																				   \n"
		"vec4 gl_Position;															   \n"
		"float gl_PointSize;														   \n"
		"float gl_ClipDistance[];													   \n"
	"} gl_out[];																	   \n"
	"																				   \n"
	"layout(location = 0) out vertex tcs_out[];										   \n"

	"uniform mat4 proj_matrix;                                                         \n"
	"                                                                                  \n"
	"void main(void)                                                                   \n"
	"{                                                                                 \n"
	"    if (gl_InvocationID == 0)                                                     \n"
	"    {                                                                             \n"
	"        gl_TessLevelInner[0] = 9.0;											   \n"
	"        gl_TessLevelOuter[0] = 3.0;                                               \n"
	"        gl_TessLevelOuter[1] = 5.0;                                               \n"
	"        gl_TessLevelOuter[2] = 3.0;                                               \n"
	"    }                                                                             \n"
	"                                                                                  \n"
	"    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;     \n"
	"	 tcs_out[gl_InvocationID].mv_matrix = mv_matrix[gl_InvocationID];		   \n"		
	"}                                                                                 \n"
};

static const char * tes_source[] =
{
	"#version 460 core                                                                 \n"
	"                                                                                  \n"
	"layout (triangles, equal_spacing, cw) in;										   \n"
	"                                                                                  \n"
	"struct vertex { mat4 mv_matrix; };												   \n"
	"																				   \n"
	"in gl_PerVertex																   \n"
	"{																				   \n"
		"vec4 gl_Position;															   \n"
		"float gl_PointSize;														   \n"
		"float gl_ClipDistance[];													   \n"
	"} gl_in[];																	       \n"
	"																				   \n"
	"																				   \n"
	"layout(location = 0) in vertex tcs_out[];                                        \n"
	"uniform mat4 proj_matrix;                                                         \n"
	"                                                                                  \n"
	"out gl_PerVertex																   \n"
	"{																				   \n"
		"vec4 gl_Position;															   \n"
		"float gl_PointSize;														   \n"
		"float gl_ClipDistance[];													   \n"
	"};																				   \n"
	"																				   \n"
	"out vec3 normal;                                             \n"
	"                                                                                  \n"
	"void main(void)                                                                   \n"
	"{                                                                                 \n"
	 "    vec4 pos =  (gl_TessCoord.x * gl_in[0].gl_Position) +						   \n"
			"                  (gl_TessCoord.y * gl_in[1].gl_Position) +               \n"
			"                  (gl_TessCoord.z * gl_in[2].gl_Position);                \n"
	"	gl_Position = tcs_out[0].mv_matrix * pos;									   \n"
	"}                                                                                 \n"
};

static const char * fs_source[] =
{
	"#version 460 core                                                  \n"
	"                                                                   \n"
	"in vec3 normal;								\n"
	"out vec4 color;                               \n"
	"                                                                   \n"
	"void main(void)                                                    \n"
	"{                                                                  \n"
	"    color = vec4(0.0, 0.8, 1.0, 1.0);                              \n"
	"}                                                                  \n"
};

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "Tessellated Instance Example", nullptr, nullptr);

	if (window == nullptr)
	{
		std::cout << "Failed to create window" << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebufferResizeCallback);
	glfwSetKeyCallback(window, glfwKeyCallback);
	glfwSetErrorCallback(glfwErrorCallback);

	glewExperimental = true;
	if (glewInit() != GLEW_OK)
	{
		std::cout << "Failed to initialize glew" << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glEnable(GL_DEBUG_OUTPUT);
	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(glewErrorCallback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GLFW_TRUE);

	int success;
	char infoLog[512];

	uint32_t program = glCreateProgram();
	
	uint32_t fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, fs_source, nullptr);
	glCompileShader(fs);

	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fs, 512, nullptr, infoLog);
		std::cout << "Error compiling fs shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	uint32_t vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, vs_source, nullptr);
	glCompileShader(vs);

	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vs, 512, nullptr, infoLog);
		std::cout << "Error compiling vs shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	uint32_t tcs = glCreateShader(GL_TESS_CONTROL_SHADER);
	glShaderSource(tcs, 1, tcs_source, nullptr);
	glCompileShader(tcs);

	glGetShaderiv(tcs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(tcs, 512, nullptr, infoLog);
		std::cout << "Error compiling tcs shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	uint32_t tes = glCreateShader(GL_TESS_EVALUATION_SHADER);
	glShaderSource(tes, 1, tes_source, nullptr);
	glCompileShader(tes);

	glGetShaderiv(tes, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(tes, 512, nullptr, infoLog);
		std::cout << "Error compiling tes shader\n" << infoLog << std::endl;
		getchar();
		glfwTerminate();
		return -1;
	}

	glAttachShader(program, vs);
	glAttachShader(program, tcs);
	glAttachShader(program, tes);
	glAttachShader(program, fs);

	glLinkProgram(program);

	glDeleteShader(vs);
	glDeleteShader(tcs);
	glDeleteShader(tes);
	glDeleteShader(fs);

	uint32_t vao, mb;
	
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	std::vector<glm::mat4> locations;
	locations.push_back(glm::translate(glm::mat4(1), glm::vec3(0.5, 0.5f, 0.0f)));
	locations.push_back(glm::translate(glm::mat4(1), glm::vec3(-0.5, 0.5f, 0.0f)));
	locations.push_back(glm::translate(glm::mat4(1), glm::vec3(0.5, -0.5f, 0.0f)));
	locations.push_back(glm::translate(glm::mat4(1), glm::vec3(-0.5, -0.5f, 0.0f)));


	glGenBuffers(1, &mb);
	glBindBuffer(GL_ARRAY_BUFFER, mb);
	glBufferData(GL_ARRAY_BUFFER, locations.size() * sizeof(glm::mat4), &locations[0], GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)0);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(sizeof(glm::vec4)));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(2 * sizeof(glm::vec4)));
	glEnableVertexAttribArray(2);

	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), (void*)(3 * sizeof(glm::vec4)));
	glEnableVertexAttribArray(3);

	glVertexAttribDivisor(0, 1);
	glVertexAttribDivisor(1, 1);
	glVertexAttribDivisor(2, 1);
	glVertexAttribDivisor(3, 1);

	while (!glfwWindowShouldClose(window))
	{
		processInput(window);

		glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(program);

		glPolygonMode(GL_FRONT_AND_BACK, wireFrameEnabled ? GL_LINE : GL_FILL);

		//glDrawArrays(GL_PATCHES, 0, 3);
		glDrawArraysInstanced(GL_PATCHES, 0, 3, locations.size());

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &vao);
	glDeleteProgram(program);
	glDeleteBuffers(1, &mb);
	
	glfwTerminate();
	return 0;
}

void framebufferResizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void glfwKeyCallback(GLFWwindow* window, int key, int scanCode, int action, int mods)
{
	if(key == GLFW_KEY_W && action == GLFW_PRESS)
	{
		wireFrameEnabled ^= 1;
		std::cout << "Wireframe Mode Toggle: " << wireFrameEnabled << std::endl;
	}
}

void glewErrorCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message,
	const GLvoid* userParam)
{
	// ignore non-significant error/warning codes
	if (id == 131169 || id == 131185 || id == 131218 || id == 131204)
		return;

	const auto startsWith = [](const char *str, const char* pre)
	{
		const auto lenpre = strlen(pre);
		const auto lenstr = strlen(str);
		return lenstr < lenpre ? false : strncmp(pre, str, lenpre) == 0;
	};

	// skip shader recompile performance warning on Intel chips
	// TODO: what does this message mean ... there is no information about this warning available online
	if (source == GL_DEBUG_SOURCE_API && type == GL_DEBUG_TYPE_PERFORMANCE && severity == GL_DEBUG_SEVERITY_MEDIUM && startsWith(message, "API_ID_RECOMPILE_FRAGMENT_SHADER"))
		return;

	std::stringstream msg;

	msg << "GL Error Callback: ID=" << id << ": " << message;

	int errorId = id;
	int errorSource = 0;
	int errorType = 0;
	int errorSeverity = 0;

	msg << ", ";
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:             msg << "Source: API"; errorSource = 1; break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   msg << "Source: Window System"; errorSource = 2; break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER: msg << "Source: Shader Compiler"; errorSource = 3; break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:     msg << "Source: Third Party"; errorSource = 4; break;
	case GL_DEBUG_SOURCE_APPLICATION:     msg << "Source: Application"; errorSource = 5; break;
	case GL_DEBUG_SOURCE_OTHER:           msg << "Source: Other"; errorSource = 6; break;
	}

	msg << ", ";
	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:				msg << "Type: Error"; errorType = 1; break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: msg << "Type: Deprecated Behaviour"; errorType = 2; break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  msg << "Type: Undefined Behaviour"; errorType = 3; break;
	case GL_DEBUG_TYPE_PORTABILITY:         msg << "Type: Portability"; errorType = 4; break;
	case GL_DEBUG_TYPE_PERFORMANCE:         msg << "Type: Performance"; errorType = 5; break;
	case GL_DEBUG_TYPE_MARKER:              msg << "Type: Marker"; errorType = 6; return;			// ignore marker, push group & pop group messages
	case GL_DEBUG_TYPE_PUSH_GROUP:          msg << "Type: Push Group"; errorType = 7; return;		// are used to mark specific stages of the rendering (eg. renderdoc)
	case GL_DEBUG_TYPE_POP_GROUP:           msg << "Type: Pop Group"; errorType = 8; return;		// but they aren't actually error messages
	case GL_DEBUG_TYPE_OTHER:				msg << "Type: Other"; errorType = 9; break;
	}

	msg << ", ";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:         msg << "Severity: high"; errorSeverity = 4; break;
	case GL_DEBUG_SEVERITY_MEDIUM:       msg << "Severity: medium"; errorSeverity = 3; break;
	case GL_DEBUG_SEVERITY_LOW:          msg << "Severity: low"; errorSeverity = 2; break;
	case GL_DEBUG_SEVERITY_NOTIFICATION: msg << "Severity: notification"; errorSeverity = 1; break;
	}

	// todo find out how "severe" this really is
	bool logNotifications = true;
	if (severity == GL_DEBUG_SEVERITY_NOTIFICATION && !logNotifications)
		return;

	std::cout << msg.str() << std::endl;

}

void glfwErrorCallback(int error, const char* description)
{
	std::cout << "GLFW error: " << error << " \n: " << description;
}
